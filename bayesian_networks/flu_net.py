from bayes_net import BayesNet, T, F, ProbDist, globalize, enumeration_ask

flu_net = (BayesNet()
           .add('Vaccinated', [], 0.6)
           .add('Flu', ['Vaccinated'], {T: 0.002, F: 0.02})
           .add('Fever', ['Flu'], {T: ProbDist(no=25, mild=25, high=50),
                                   F: ProbDist(no=97, mild=2, high=1)})
           .add('Headache', ['Flu'], {T: 0.5,   F: 0.03}))

globalize(flu_net.lookup)

Vaccinated = flu_net.variables[0]
Flu = flu_net.variables[1]
Fever = flu_net.variables[2]
Headache = flu_net.variables[3]

#print(joint_distribution(flu_net))
# If you just have a headache, you probably don't have the Flu.
print(enumeration_ask(Flu, {Headache: T, Fever: 'no'}, flu_net))
# {F: 0.9616440110625343, T: 0.03835598893746573}

# Even more so if you were vaccinated.
print(enumeration_ask(Flu, {Headache: T, Fever: 'no', Vaccinated: T}, flu_net))
# {F: 0.9914651882096696, T: 0.008534811790330398}

# But if you were not vaccinated, there is a higher chance you have the flu.
print(enumeration_ask(Flu, {Headache: T, Fever: 'no', Vaccinated: F}, flu_net))
# {F: 0.9194016377587207, T: 0.08059836224127925}

# And if you have both headache and fever, and were not vaccinated,
# then the flu is very likely, especially if it is a high fever.
print(enumeration_ask(Flu, {Headache: T, Fever: 'mild', Vaccinated: F}, flu_net))
# {F: 0.19041450777202068, T: 0.8095854922279793}

print(enumeration_ask(Flu, {Headache: T, Fever: 'high', Vaccinated: F}, flu_net))
# {F: 0.05553456743483188, T: 0.9444654325651682}
